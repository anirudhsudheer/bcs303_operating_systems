#include <stdio.h>

#define MAX_PROCESSES 10
#define MAX_RESOURCES 10

int available[MAX_RESOURCES];
int maximum[MAX_PROCESSES][MAX_RESOURCES];
int allocation[MAX_PROCESSES][MAX_RESOURCES];
int need[MAX_PROCESSES][MAX_RESOURCES];

int processes, resources;

void input()
{
    int i, j;

    printf("Enter the number of processes: ");
    scanf("%d", &processes);

    printf("Enter the number of resources: ");
    scanf("%d", &resources);

    printf("Enter the available resources: ");
    for (j = 0; j < resources; j++)
        scanf("%d", &available[j]);

    printf("Enter the maximum matrix:\n");
    for (i = 0; i < processes; i++)
        for (j = 0; j < resources; j++)
            scanf("%d", &maximum[i][j]);

    printf("Enter the allocation matrix:\n");
    for (i = 0; i < processes; i++)
        for (j = 0; j < resources; j++)
        {
            scanf("%d", &allocation[i][j]);
            need[i][j] = maximum[i][j] - allocation[i][j];
        }
}

int safety_check(int process, int work[], int finish[])
{
    int j;

    for (j = 0; j < resources; j++)
        if (need[process][j] > work[j])
            return 0;

    for (j = 0; j < resources; j++)
    {
        work[j] += allocation[process][j];
        finish[process] = 1;
    }

    return 1;
}

void bankers_algorithm()
{
    int work[MAX_RESOURCES], finish[MAX_PROCESSES];
    int i, process, count = 0;

    for (i = 0; i < resources; i++)
        work[i] = available[i];

    for (i = 0; i < processes; i++)
        finish[i] = 0;

    while (count < processes)
    {
        int found = 0;

        for (process = 0; process < processes; process++)
        {
            if (finish[process] == 0 && safety_check(process, work, finish))
            {
                printf("Process %d is in safe state\n", process);
                found = 1;
                finish[process] = 1;
                count++;
            }
        }

        if (!found)
        {
            printf("System is not in a safe state. Deadlock detected.\n");
            break;
        }
    }
}

int main()
{
    input();
    bankers_algorithm();

    return 0;
}

