#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#define FIFO_NAME "/tmp/myfifo"

int main() {
    // Open the FIFO for reading
    int fd;
    if ((fd = open(FIFO_NAME, O_RDONLY)) == -1) {
        perror("Error opening FIFO for reading");
        exit(EXIT_FAILURE);
    }

    // Read and execute five bash commands from the FIFO
    for (int i = 0; i < 5; ++i) {
        char command[100];
        read(fd, command, sizeof(command));
        printf("Reader process received command: %s\n", command);

        // Execute the command using system()
        system(command);
    }

    // Close the FIFO
    close(fd);
    exit(EXIT_SUCCESS);
}

