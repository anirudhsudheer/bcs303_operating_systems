#include <stdio.h>
#include <string.h>
#include <sys/stat.h>  // For Unix-like systems
#include <sys/types.h>


#define MAX_DIRECTORIES 10
#define MAX_FILES 10

struct File {
    char name[20];
    int size;
};

struct Directory {
    char name[20];
    struct File files[MAX_FILES];
    int fileCount;
};

struct TwoLevelDirectory {
    struct Directory directories[MAX_DIRECTORIES];
    int directoryCount;
};

void initializeTwoLevelDirectory(struct TwoLevelDirectory *directory) {
    directory->directoryCount = 0;
}

void addDirectory(struct TwoLevelDirectory *directory, const char *name) {
    if (directory->directoryCount < MAX_DIRECTORIES) {
        strcpy(directory->directories[directory->directoryCount].name, name);
        directory->directories[directory->directoryCount].fileCount = 0;
        directory->directoryCount++;
        printf("Directory '%s' added successfully.\n", name);

        // Create a physical directory on the file system
        mkdir(name, 0777);
    } else {
        printf("Cannot add more directories. Limit reached.\n");
    }
}

void addFileToDirectory(struct TwoLevelDirectory *directory, const char *directoryName, const char *fileName, int size) {
    for (int i = 0; i < directory->directoryCount; i++) {
        if (strcmp(directory->directories[i].name, directoryName) == 0) {
            if (directory->directories[i].fileCount < MAX_FILES) {
                strcpy(directory->directories[i].files[directory->directories[i].fileCount].name, fileName);
                directory->directories[i].files[directory->directories[i].fileCount].size = size;
                directory->directories[i].fileCount++;
                printf("File '%s' added to directory '%s' successfully.\n", fileName, directoryName);

                // Create a physical file on the file system
                char filePath[50];
                sprintf(filePath, "%s/%s", directoryName, fileName);
                FILE *file = fopen(filePath, "w");
                fclose(file);
            } else {
                printf("Directory is full. Cannot add more files.\n");
            }
            return;
        }
    }
    printf("Directory '%s' not found.\n", directoryName);
}

void displayTwoLevelDirectory(struct TwoLevelDirectory *directory) {
    printf("Two Level Directory:\n");
    for (int i = 0; i < directory->directoryCount; i++) {
        printf("Directory Name: %s\n", directory->directories[i].name);
        for (int j = 0; j < directory->directories[i].fileCount; j++) {
            printf("\tFile Name: %s, Size: %d KB\n", directory->directories[i].files[j].name, directory->directories[i].files[j].size);
        }
    }
}

int main() {
    struct TwoLevelDirectory directory;
    initializeTwoLevelDirectory(&directory);

    addDirectory(&directory, "docs");
    addDirectory(&directory, "images");

    addFileToDirectory(&directory, "docs", "document1.txt", 50);
    addFileToDirectory(&directory, "docs", "document2.txt", 75);
    addFileToDirectory(&directory, "images", "image1.jpg", 200);

    displayTwoLevelDirectory(&directory);

    return 0;
}

